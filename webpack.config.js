const autoprefixer = require('autoprefixer');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    mode: 'development',

    entry: './src/main.js',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/dist',
    },

    module: {
        rules: [
            { 
                test: /\.vue$/, 
                use: 'vue-loader' 
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'vue-style-loader' },
                    { loader: 'css-loader' },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                autoprefixer({
                                    browsers:['ie >= 8', 'last 4 version'],
                                })
                            ],
                            sourceMap: true,
                        },
                    },
                    { 
                        loader: 'sass-loader',
                        options: {
                            implementation: require("sass"),
                        }
                    },
                ],
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                }
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: '/dist/',
                },
            },
        ],
    },

    plugins: [
        new VueLoaderPlugin(),
        new BrowserSyncPlugin({
                host: 'localhost',
                port: 3000,
                files: ['index.html'],
                server: { baseDir: ['./']}
            },
            {reload: true},
        ),
    ]
};
