import Vue from 'vue';
import VueRouter from 'vue-router';

import payments from './components/payments.vue';
import paymentsHistory from './components/paymentsHistory.vue';
import userGreeting from './components/userGreeting.vue';
import paymentSuccess from './components/paymentSuccess.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { 
            path: '/payments', 
            name : 'payments',
            component: payments,
        },
        { 
            path: '/history',
            name: 'history', 
            component: paymentsHistory,
        },
        {
            path: '/payment-success/:account/:payment/:owner/:date', 
            name: 'payment-success',
            props: true,
            component: paymentSuccess, 
        },
        {
            path: '/payment-success',
            redirect: 'payments',
        },
        { path: '*', component: userGreeting }
    ]
})

export default router;