import { requiredField, onlyLatinChars, minLength, fixedLength } from '../utils/validations.js';

export default {
    data() {
        return {
            accountNumer: {
                name: 'account_number',
                value: null,
                checks: [ 
                    { func: requiredField }
                ],
            },
            paymentAmount: {
                name: 'payment_amount',
                value: 0,
                checks: [ 
                    { func: requiredField }
                ],
            },
            cardNumberQuad1: {
                name: 'quad1',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: fixedLength, options: 4 }, 
                ],
            },
            cardNumberQuad2: {
                name: 'quad2',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: fixedLength, options: 4 }, 
                ],
            },
            cardNumberQuad3: {
                name: 'quad3',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: fixedLength, options: 4 }, 
                ],
            },
            cardNumberQuad4: {
                name: 'quad4',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: fixedLength, options: 4 }, 
                ],
            },
            validityPeriodMonth: {
                name: 'month',
                value: null,
                checks: [ 
                    { func: requiredField },
                ],
            },
            validityPeriodYear: {
                name: 'year',
                value: null,
                checks: [ 
                    { func: requiredField },
                ],
            },
            cardOwner: {
                name: 'card_owner',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: onlyLatinChars },
                    { func: minLength, options: 4, }
                ],
            },
            cvv: {
                name: 'cvv',
                value: null,
                checks: [ 
                    { func: requiredField },
                    { func: fixedLength, options: 3 }, 
                ],
            },
            months: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ],
            years: [ 2019, 2020, 2021, 2022, 2023, 2024 ],
        }
    }
}