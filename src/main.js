import Vue from "vue";
import App from "./App.vue";
import router from "./router.js";

require('./styles/style.scss');

new Vue({
  el: "#app",
  render: h => h(App),
  router,
});
