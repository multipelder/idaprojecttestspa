const validations = {

    requiredField: function(value) {
        return !!value;
    },

    onlyLatinChars: function(value) {
        if (checkNullUndefined(value)) {
            return false;
        }

        const latinChars = 'abcdefghijklmnopqrstuvwxyz';
        const spaceChar = ' ';

        const allowedChars = latinChars + spaceChar;
        
        for (let i=0; i < value.length; i++) {
            let char = value[i];
            if (allowedChars.indexOf(char.toLowerCase()) === -1) {
                return false;
            }
        }

        return true;
    },

    minLength: function(value, min) {
        if (checkNullUndefined(value)) {
            return false;
        }
        let strValue = value.toString();
        return strValue.length >= min;
    },

    fixedLength: function(value, length) {
        if (checkNullUndefined(value)) {
            return false;
        }
        let strValue = value.toString();
        return strValue.length === length;
    },
};

function checkNullUndefined(value) {
    return value === null || value === undefined;
}

module.exports = validations;